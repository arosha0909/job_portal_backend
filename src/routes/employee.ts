import { Express } from "express";
import { EmployerEp } from "../end-point/employer-ep";

export function initEmployeeRoutes(app: Express) {

    // ------- PUBLIC ROUTES ---------

    // search company with filters and keyword
    app.get('/api/public/employee/search/filters', EmployerEp.searchEmployees);

}
