import { Express } from "express";
import { ApplicationEP } from "../end-point/application-ep";
import Application from "../schemas/application-schema";

export function initApplicationRoutes(app: Express) {
    
    // ------ EMPLOYEE ROUTES --------

    // view all applications for the vacancy posted by employee
    app.get('/api/employee/vacancy/:vid/application', ApplicationEP.viewApplicationsOfVacancyPostedByEmployee);

    // view a application sent for a vacancy posted by employee
    app.get('/api/employee/vacancy/:vid/application/:aid', ApplicationEP.viewApplicationOfVacancyPostedByEmployee);
}
