import { Express } from "express";
import { JobSeekerEp } from "../end-point/job-seeker-ep";

export function initJobSeekerRoutes(app: Express) {

    // ------- PUBLIC ROUTES ---------

    // search company with filters and keyword
    app.get('/api/public/jobseeker/search/filters', JobSeekerEp.searchJobseekers);

}
