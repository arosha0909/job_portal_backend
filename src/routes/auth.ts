import { Express } from "express";
import { PasswordResetEp } from "../end-point/password-reset-ep";
import { UserEp } from "../end-point/user-ep";

export function initAuthRoutes(app: Express) {
    // PUBLIC ROUTES
    app.post('/api/public/login', UserEp.authenticate);
    app.post('/api/public/register', UserEp.register);
    app.post('/api/public/forgot-password', PasswordResetEp.forgotPassword);
    app.post('/api/public/reset-password', PasswordResetEp.resetPassword);
    app.post('/api/public/token-validate:token', PasswordResetEp.tokenValidate);

    // AUTH ROUTES
    app.get('/api/auth/self', UserEp.getSelf);
    app.post('/api/auth/update-user', UserEp.updateUser);




}
