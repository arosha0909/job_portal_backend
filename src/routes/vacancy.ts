import { Express } from "express";
import { VacancyEP } from "../end-point/vacancy-ep";

export function initVacancyRoutes(app: Express) {

    // ------- PUBLIC ROUTES ---------
    // view all vacancies
    app.get('/api/public/vacancy', VacancyEP.viewVacancies);
    // view a vacancy : vacancy id
    app.get('/api/public/vacancy/:vid', VacancyEP.viewVacancy);
    // search vacancies with filters and keyword
    app.get('/api/public/vacancy/search/filters', VacancyEP.searchVacancies);

    
    // ------ EMPLOYEE ROUTES --------
    // view all vacancies of current logged in employee
    app.get('/api/employee/vacancy', VacancyEP.viewVacanciesOfLoggedInEmployee);
    // view a vacancy of current logged in employee
    app.get('/api/employee/vacancy/:vid', VacancyEP.viewVacancyOfLoggedInEmployee);
    // create vacancy
    app.post('/api/employee/vacancy/create', VacancyEP.createVacancy);
    // update vacancy
    app.put('/api/employee/vacancy/:vid', VacancyEP.updateVacancy);

}
