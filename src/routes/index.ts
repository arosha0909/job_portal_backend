import {Express, Request, Response} from "express";
import {Util} from "../common/util";
import {initAuthRoutes} from "./auth";
import {initUploadRoutes} from "./upload";
import { initVacancyRoutes } from "./vacancy";
import { initApplicationRoutes } from "./application";
import { initEmployeeRoutes } from "./employee";
import { initJobSeekerRoutes } from "./jobseeker";

export function initRoutes(app: Express) {
    /* TOP LEVEL */
    app.get('/api', (req: Request, res: Response) => Util.sendSuccess(res, "Photography™ Api"));

    initAuthRoutes(app);
    initUploadRoutes(app);
    // vacancy routes
    initVacancyRoutes(app);
    // employee routes
    initEmployeeRoutes(app);
    // employee routes
    initJobSeekerRoutes(app);
    // application routes
    initApplicationRoutes(app);

    /* ALL INVALID REQUESTS */
    app.all('*', (req: Request, res: Response) => Util.sendError(res, "Route Not Found"));
}
