import * as mongoose from "mongoose";

interface CommonAttributes {
    addressOne: string;
    addressTwo: string;
    city: string;
    province: string;
    zip: string;
}

export interface DAddress extends CommonAttributes { }

export interface IAddress extends CommonAttributes, mongoose.Document { }