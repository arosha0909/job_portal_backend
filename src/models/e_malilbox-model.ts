const mongoose = require("mongoose");

const E_mailbox = mongoose.model(
    "E_mailbox",
    new mongoose.Schema({
    employee : [
        {
            type:mongoose.Schema.Types.ObjectId,
            /// <reference path="Employee" />
            ref :"Employee"           
        }
    ],
    email: String,
    subject: String,
    body: String,
    attachment: File,
    })
);

module.exports = E_mailbox;
