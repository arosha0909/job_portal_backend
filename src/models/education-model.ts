import * as mongoose from "mongoose";
import { ObjectIdOr, StringOrObjectId } from "../common/util";
import { IUser } from "./user-model";

interface CommonAttributes {
    titel?: string;
    collage?: string;
    gpa?: string;
    stratDate?: Date;
    EndDate?: Date;
}

export interface DEducation extends CommonAttributes { user: StringOrObjectId; }

export interface IEducation extends CommonAttributes, mongoose.Document { user: ObjectIdOr<IUser>; }
