import * as mongoose from "mongoose";

interface CommonAttributes {
    titel?: string;
    company?: string;
    contactName?: string;
    contactNum?: string;
    startDate?: Date;
    endDate?: Date;
}

export interface DCareer extends CommonAttributes {

}

export interface ICareer extends CommonAttributes, mongoose.Document {

}
