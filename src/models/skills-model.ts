import { DUser, IUser } from "./user-model";
import * as mongoose from "mongoose";

interface CommonAttributes {
    skillName?: string;
}

export interface DSkill extends CommonAttributes {

}

export interface ISkill extends CommonAttributes, mongoose.Document {

}
