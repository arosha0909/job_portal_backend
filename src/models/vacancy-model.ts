import * as mongoose from "mongoose";
import { ObjectIdOr, StringOrObjectId } from "../common/util";
import { IAddress } from "./address-model";
import { IUser } from "./user-model";

export interface CommonAttributes {
    jobTitle: string;
    jobCategory: string;
    state: boolean;
    status: boolean;
    description: string;
    expirience: number;
    salary: number;
    postDate: Date;
    endDate: Date;
}

// for filter search
export interface FVacancy {
    jobTitle: string;
    jobCategory: string;
    description: string;
    expirience: number;
    salary: number;
    postDate: Date;
    endDate: Date;
}

export interface DVacancy extends CommonAttributes {
    user: StringOrObjectId;
    address: StringOrObjectId;
}

export interface IVacancy extends CommonAttributes, mongoose.Document {
    user: ObjectIdOr<IUser>;
    address: ObjectIdOr<IAddress>;
}

