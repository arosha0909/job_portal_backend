import * as mongoose from "mongoose";
import { ObjectIdOr, StringOrObjectId } from "../common/util";
import { IUser } from "./user-model";
import { IVacancy } from "./vacancy-model";

export interface DApplication {
    vacancy: StringOrObjectId;
    user: StringOrObjectId;
}

export interface IApplication extends mongoose.Document {
    Vacancy: ObjectIdOr<IVacancy>;
    user: ObjectIdOr<IUser>;
}