import * as mongoose from "mongoose";

interface CommonAttributes {
    email: string;
    token: string;
}

export interface DPasswordReset extends CommonAttributes {}

export interface IPasswordReset extends CommonAttributes, mongoose.Document {}