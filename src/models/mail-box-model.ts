import { ObjectIdOr, StringOrObjectId } from "../common/util";
import { IUser } from "./user-model";
import * as mongoose from "mongoose";

export interface CommonAttributes {
    email: string;
    subject: string;
    body: string;
    attachments?: string[];
}

export interface DMailBox extends CommonAttributes {
    user: StringOrObjectId;
}

export interface IMailBox extends CommonAttributes, mongoose.Document {
    user: ObjectIdOr<IUser>;
}