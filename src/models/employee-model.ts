import * as mongoose from "mongoose";
import { ObjectId } from "mongoose";
import { ObjectIdOr, StringOrObjectId } from "../common/util";
import { IAddress } from "./address-model";
import { DUser, IUser } from "./user-model";

interface CommonAttributes {
    industry?: string;
    name?: string;
    contacatNo?: string;
    aboutCompany?: string;
    website?: string;
}

// for filter search
export interface FEmployee {
    industry?: string;
}

export interface DEmployer extends CommonAttributes, DUser { address?: StringOrObjectId; }

export interface IEmployer extends CommonAttributes, IUser, mongoose.Document { address?: ObjectIdOr<IAddress> }
