import { ObjectIdOr, StringOrObjectId } from "../common/util";
import { IAddress } from "./address-model";
import { IEducation } from "./education-model";
import { ISkill } from "./skills-model";
import { IUpload } from "./upload-model";
import { ICareer } from "./career-model";
import { DUser, IUser } from "./user-model";

interface CommonAttributes {
    firstName: string;
    lastName: string;
    contact: string;
    dob: Date;
    age: number;
    aboutMe?: string;
    gender: string;
}

export interface FJobSeeker {
    age: number;
    gender: string;
}

export interface DJobSeeker extends CommonAttributes, DUser {
    career?: StringOrObjectId[];
    eduacation?: StringOrObjectId[];
    skill?: StringOrObjectId[];
    address?: StringOrObjectId;
    resumeUpload?: StringOrObjectId;
    coverLatter?: StringOrObjectId;
}

export interface IJobSeeker extends CommonAttributes, IUser {
    career?: ObjectIdOr<ICareer>[];
    eduaction?: ObjectIdOr<IEducation>[];
    skill?: ObjectIdOr<ISkill>[];
    address?: ObjectIdOr<IAddress>;
    resumeUpload?: ObjectIdOr<IUpload>;
    coverLatter?: ObjectIdOr<IUpload>;
}
