import * as mongoose from "mongoose";
import { ObjectIdOr, StringOrObjectId } from "../common/util";
import { IUser } from "./user-model";

export interface CommonAttributes {
    amount: number;
    date: Date;
    dueDate: Date;
}

export interface DSubscription extends CommonAttributes {
    user: StringOrObjectId;
}

export interface ISubscription extends CommonAttributes, mongoose.Document {
    user: ObjectIdOr<IUser>;
}
