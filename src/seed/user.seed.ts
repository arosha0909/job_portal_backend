import { DUser, Role } from "../models/user-model";
import { AdminDao } from "../dao/admin-dao";

export const superAdminEmail = `superadmin@tests.com`;

export default async function seedUsers() {
    const superAdmin: DUser = {
        password: "111111",
        email: superAdminEmail,
        role: Role.SUPER_ADMIN,
        lastLogin: new Date()
    };
    const superAdmin_ = await createAdmin(superAdmin);
}

async function createAdmin(user: DUser) {
    const existingUser = await AdminDao.getAdminByEmail(user.email);
    if (existingUser) {
        return existingUser;
    }
    return await AdminDao.createAdmin(user);
}
