import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";
import { ApplicationError } from "../common/application-error";
import { Util } from "../common/util";
import { Validation } from "../common/validation";
import { UserDao } from "../dao/user-dao";
import * as jwt from "jsonwebtoken";
import { JwtToken } from "../middleware/jwtToken";
import { DPasswordReset } from "../models/password-reset-model";
import { PasswordResetDao } from "../dao/password-reset-dao";
import { AppLogger } from "../common/logging";
import User from "../schemas/user-schema";

export namespace PasswordResetEp {

    export function frogotPasswordValidationRules() {
        return [Validation.email()];
    }

    export function resetPasswordValidationRules() {
        return [Validation.password()];
    }

    export function tokenValidationRules() {
        return [Validation.text('token')];
    }

    export async function forgotPassword(req: Request, res: Response, next: NextFunction) {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return Util.sendError(res, errors);
        }

        const email: string = req.body.email;

        UserDao.getUserByEmail(email).then((user: any) => {
            if (!user) {
                throw new ApplicationError('User not found');
            }

            const token: string = JwtToken.createPasswordToken(email);
            const entry: DPasswordReset = {email: email, token: token};

            PasswordResetDao.addEmailAndToken(entry).then(forgotPassword => {
                if (forgotPassword) {
                    // write code to send password reste email to user
                    // code will like a bellow

                    // emailFile.emailfunc(mail, token).then(() => {
                    //     Util.sendSuccess(res, forgotPassword, 'and sucess text here')
                    // }).catch((error) => {
                    //     Util.dendError(res, 'error occured while sending mail');
                    //     AppLogger.error(error)
                    // })
                }
            }).catch(next);
        })
    }

    export async function resetPassword(req: Request, res: Response, next: NextFunction) {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return Util.sendError(res, errors);
        }

        const remember = !!req.body.email;
        const token = req.body.token;
        const password = req.body.password;

        let email: string;

        jwt.verify(token, process.env.JWT_SECRET, async (err: any, decoded: any) => {
            if (err) {
                return Util.sendError(res, 'fail to authentication email');
            } else {
                email = decoded.email;
                const user = await UserDao.getUserByEmail(email);

                if (user) {
                    await UserDao.updateUser(user.id, {password: password});
                } else {
                    return Util.sendError(res, 'no valid user found');
                }

                UserDao.authenticateUser(email, password, remember).then((token: string) => {
                    Util.sendSuccess(res, token);
                }).catch(next);
            }
        })

    }

    export async function tokenValidate(req: Request, res: Response, next: NextFunction) {
        const token = req.params.token;
        let decode: {email: string} = {email: ''};

        jwt.verify(token, process.env.JWT_SECRET, (err: any, decoded: any) => {
            if (err) {
                return Util.sendError(res, 'failed to authentication token');
            } else {
                decode = decoded;
            }
        });

        if (!decode.email) {
            return Util.sendError(res, 'failed authenticate user');
        } else {
            PasswordResetDao.tokenValidate(decode.email, token).then(passwordReset => {
                if (!passwordReset) {
                    Util.sendError(res, 'failed to authenticate token');
                } else {
                    Util.sendSuccess(res, passwordReset);
                }
            }).catch(next);
        }
    }
}