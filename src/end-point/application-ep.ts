import { NextFunction, Request, Response } from "express";
import { ApplicationDao } from "../dao/application-dao";
import { Util } from "../common/util";

export namespace ApplicationEP {
    
    // employee --- view all applications of the vacancy placed by logged in employee
    export async function viewApplicationsOfVacancyPostedByEmployee(req: Request, res: Response, next: NextFunction) {
        const vacancyID = req.params.vid;
        const userID = req.user._id;
        ApplicationDao.viewApplicationsFromVacancyAndEmployee(userID, vacancyID).then((data:any) => Util.sendSuccess(res, data)).catch(next);
    }

    // employee --- view details of a application sent for a vacancy placed by the logged in employee
    export async function viewApplicationOfVacancyPostedByEmployee(req: Request, res: Response, next: NextFunction) {
        const vacancyID = req.params.vid;
        const applicationID = req.params.aid;
        const userID = req.user._id;
        ApplicationDao.viewApplicationFromVacancyAndEmployee(userID, vacancyID, applicationID).then((data:any) => Util.sendSuccess(res, data)).catch(next);
    }
}