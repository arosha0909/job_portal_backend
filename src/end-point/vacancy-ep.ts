import { Validation } from "../common/validation";
import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";
import { Util } from "../common/util";
import { VacancyDao } from "../dao/vacancy-dao";
import { DVacancy } from "../models/vacancy-model";

export namespace VacancyEP {
    export function jobPostValidationRules() {
        return [Validation.text("jobTitle"),
            Validation.text("description"),
             Validation.text("expirience")]
    }

    // employee
    export async function createVacancy(req: Request, res: Response, next: NextFunction) {
        const errors = validationResult(req);

        if (!errors.isEmpty())
            return Util.sendError(res, errors.array()[0]['msg']);
              
        VacancyDao.createVacancy(<DVacancy>req.body).then((data:any)=>Util.sendSuccess(res, data)).catch(next);
    }

    // employee
    export async function updateVacancy(req: Request, res: Response, next: NextFunction) {
        const errors = validationResult(req);

        if (!errors.isEmpty())
            return Util.sendError(res, errors.array()[0]['msg']);
        
        // get vacancy ID
        const vacancyID = req.params.vid;

        // get user ID
        const userID = req.user._id;
        
        VacancyDao.updateVacancy(vacancyID, userID, req.body).then((data:any)=>Util.sendSuccess(res, data)).catch(next);
    }

    // employee
    export async function viewVacanciesOfLoggedInEmployee(req: Request, res: Response, next: NextFunction) {
        const userID = req.user._id;
        VacancyDao.viewVacanciesPostedByEmployee(userID).then((data:any) => Util.sendSuccess(res, data)).catch(next);
    }

    // employee
    export async function viewVacancyOfLoggedInEmployee(req: Request, res: Response, next: NextFunction) {
        const userID = req.user._id;
        const vacancyID = req.params.vid;
        VacancyDao.viewVacancyPostedByEmployee(userID, vacancyID).then((data:any) => Util.sendSuccess(res, data)).catch(next);
    }

    // public
    export async function searchVacancies(req: Request, res: Response, next: NextFunction) {
        const filters: any = req.query;
        const searchKeyword: string = filters.keyword;
        // set preset options
        filters.state = true;
        filters.status = true;
        delete filters.keyword;
        // fetch
        VacancyDao.searchVacancies(filters, searchKeyword).then((data:any) => Util.sendSuccess(res, data)).catch(next);
    }

    // public
    export async function viewVacancies(req: Request, res: Response, next: NextFunction) {
        VacancyDao.viewVacancies().then((data:any) => Util.sendSuccess(res, data)).catch(next);
    }

    // public
    export async function viewVacancy(req: Request, res: Response, next: NextFunction) {
        const vacancyID = req.params.vid;
        VacancyDao.viewVacancy(vacancyID).then((data:any) => Util.sendSuccess(res, data)).catch(next);
    }
    
}