import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";
import { Util } from "../common/util";
import { Validation } from "../common/validation";
import { EmployerDao } from "../dao/employer-dao";
import { JobSeekerDao } from "../dao/job-seeker-dao";
import { DEmployer, FEmployee, IEmployer } from "../models/employee-model";
import { Role } from "../models/user-model";

export namespace EmployerEp {
    export function registerValidationRules() {
        return [
            Validation.role(Role.EMPLOYER),
            Validation.email(),
            Validation.password(),
            Validation.noPermissions(),
        ];
    }

    export async function createEmployer(req: Request, res: Response, next: NextFunction) {
        const errors = validationResult(req);
        !errors.isEmpty() ? Util.sendError(res, errors.array()[0]['msg']) :
            EmployerDao.createEmployer(req.body).then((data: any) => Util.sendSuccess(res, data)).catch(next);
    }

    export async function UpdateEmployer(req: Request, res: Response, next: NextFunction) {
        const employer: DEmployer = req.body;
        const updatingEmployer: Partial<DEmployer> = {};

        if (employer.industry) {
            updatingEmployer.industry = employer.industry;
        }

        if (employer.name) {
            updatingEmployer.name = employer.name;
        }

        if (employer.contacatNo) {
            updatingEmployer.contacatNo = employer.contacatNo;
        }

        if (employer.aboutCompany) {
            updatingEmployer.aboutCompany = employer.aboutCompany;
        }

        if (employer.website) {
            updatingEmployer.website = employer.website;
        }

        if (employer.photo) {
            updatingEmployer.photo = employer.photo;
        } 

        if (employer.address) {
            updatingEmployer.address = employer.address;
        } 

        const updatedEmployer = await JobSeekerDao.updatedJobSeeker(req.body.id, updatingEmployer);
        return updatedEmployer;
    }

       // public
    export async function searchEmployees(req: Request, res: Response, next: NextFunction) {
        const filters: any = req.query;
        const searchKeyword: string = filters.keyword;
        // set preset options
        delete filters.keyword;
        // fetch
        EmployerDao.searchEmployees(filters, searchKeyword).then((data:any) => Util.sendSuccess(res, data)).catch(next);
    }

}