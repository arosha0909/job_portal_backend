import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";
import { error } from "util";
import { Util } from "../common/util";
import { Validation } from "../common/validation";
import { Role } from "../models/user-model";
import { JobSeekerDao } from "../dao/job-seeker-dao"
import { DJobSeeker } from "../models/jobSeeker-model";

export namespace JobSeekerEp {

    export function registerValidationRules() {
        return [
            Validation.role(Role.JOB_SEEKER),
            Validation.email(),
            Validation.password(),
            Validation.noPermissions(),
        ];
    }

    // public
    export async function searchJobseekers(req: Request, res: Response, next: NextFunction) {
        const filters: any = req.query;
        const searchKeyword: string = filters.keyword;
        // set preset options
        delete filters.keyword;
        // fetch
        JobSeekerDao.searchJobseekers(filters, searchKeyword).then((data:any) => Util.sendSuccess(res, data)).catch(next);
    }

    export async function createJobSeeker(req: Request, res: Response, next: NextFunction) {
        const errors = validationResult(req);
        !errors.isEmpty() ? Util.sendError(res, errors.array()[0]['msg']) :
            JobSeekerDao.createJobSeeker(req.body).then((data: any) => Util.sendSuccess(res, data)).catch(next);
    }

    export async function UpdateJobSeeker(req: Request, res: Response, next: NextFunction) {
        const jobSeeker: DJobSeeker = req.body;
        const updatingJobSeeker: Partial<DJobSeeker> = {};

        if (jobSeeker.firstName) {
            updatingJobSeeker.firstName = jobSeeker.firstName;
        }

        if (jobSeeker.lastName) {
            updatingJobSeeker.lastName = jobSeeker.lastName;
        }

        if (jobSeeker.contact) {
            updatingJobSeeker.contact = jobSeeker.contact;
        }

        if (jobSeeker.dob) {
            updatingJobSeeker.dob = jobSeeker.dob;
        }

        if (jobSeeker.age) {
            updatingJobSeeker.age = jobSeeker.age;
        }

        if (jobSeeker.aboutMe) {
            updatingJobSeeker.aboutMe = jobSeeker.aboutMe;
        }
        
        if (jobSeeker.gender) {
            updatingJobSeeker.gender = jobSeeker.gender;
        }
        
        if (jobSeeker.career) {
            updatingJobSeeker.career = jobSeeker.career;
        }
        
        if (jobSeeker.eduacation) {
            updatingJobSeeker.eduacation = jobSeeker.eduacation;
        }

        if (jobSeeker.skill) {
            updatingJobSeeker.skill = jobSeeker.skill;
        }

        if (jobSeeker.address) {
            updatingJobSeeker.address = jobSeeker.address;
        }

        if (jobSeeker.resumeUpload) {
            updatingJobSeeker.resumeUpload = jobSeeker.resumeUpload;
        }

        if (jobSeeker.coverLatter) {
            updatingJobSeeker.coverLatter = jobSeeker.coverLatter;
        }

        if (jobSeeker.photo) {
            updatingJobSeeker.photo = jobSeeker.photo;
        }

        const updatedJobSeeker = await JobSeekerDao.updatedJobSeeker(req.body.id, updatingJobSeeker);
        return updatedJobSeeker;
        
    }
}