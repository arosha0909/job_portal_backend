import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";
import { Util } from "../common/util";
import { UserDao } from "../dao/user-dao";
import { Validation } from "../common/validation";
import { Role } from "../models/user-model";
import { JobSeekerEp } from "./job-seeker-ep";
import { EmployerEp } from "./employer-ep";

export namespace UserEp {

    export function authValidationRules() {
        return [
            Validation.email(),
            Validation.password()
        ];
    }

    export async function authenticate(req: Request, res: Response, next: NextFunction) {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return Util.sendError(res, errors.array()[0]['msg']);
        }

        UserDao.authenticateUser(req.body.email, req.body.password).then((token: string) => {
            Util.sendSuccess(res, token);
        }).catch(next);
    }

    export async function register(req: Request, res: Response, next: NextFunction) {
        try {
            switch (req.body.role) {
                case Role.JOB_SEEKER:
                    await JobSeekerEp.createJobSeeker(req, res, next);
                    break;
                case Role.EMPLOYER:
                    await EmployerEp.createEmployer(req, res, next);
                    break;
                case Role.ADMIN: // this is not a super admin, admin has limited permission like a modarator
                    // await EmployerEp.createEmployer(req, res, next);
                    break;
                case Role.MODERATOR:
                    // await EmployerEp.createEmployer(req, res, next);
                    break;
            }
        } catch (e) {
            Util.sendError(res, e);
        }
    }

    export async function updateUser(req: Request, res: Response, next: NextFunction) {
        try {
            switch(req.body.role) {
                case Role.JOB_SEEKER:
                    const jobSeeker = await JobSeekerEp.UpdateJobSeeker(req, res, next);
                    Util.sendSuccess(res, jobSeeker);
                    break;
                case Role.EMPLOYER:
                    const employer = await EmployerEp.UpdateEmployer(req, res, next);
                    Util.sendSuccess(res, employer);
                    break;
                case Role.ADMIN:
                    // want to write
                    break;
                case Role.MODERATOR: 
                    // want to write
                    break;
            }
            
        } catch (e) {
            Util.sendError(res, e);
        }
    }

    export function getSelf(req: Request, res: Response, next: NextFunction) {
        UserDao.getUserById(req.user._id).then(user => {
            Util.sendSuccess(res, user);
        }).catch(next);
    }
}