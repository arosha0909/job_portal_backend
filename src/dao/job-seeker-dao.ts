import { Types } from "mongoose";
import { AppLogger } from "../common/logging";
import { StringOrObjectId } from "../common/util";
import { DJobSeeker, FJobSeeker, IJobSeeker } from "../models/jobSeeker-model";
import JobSeeker from "../schemas/jobSeeker-schema";
import { UserDao } from "./user-dao";

export namespace JobSeekerDao {
    export async function createJobSeeker(data: DJobSeeker): Promise<string> {
        const iJobSeeker = new JobSeeker(data);
        let jobSeeker = await iJobSeeker.save();
        AppLogger.info(`Create profile for user ID: ${jobSeeker._id}`);
        return await UserDao.authenticateUser(data.email, data.password);
    }

    export async function updatedJobSeeker(jobSeekerId: StringOrObjectId, data: Partial<DJobSeeker>): Promise<IJobSeeker> {
        // @ts-ignore
        const updatedJobSeeker = await JobSeeker.findByIdAndUpdate(Types.ObjectId(jobSeekerId), {'$set': data}, {new: true});
        AppLogger.info(`update job seeker for ID: ${updatedJobSeeker.id}`);
        return updatedJobSeeker;
    }

    // public
    export async function searchJobseekers(filters: Partial<FJobSeeker>, keyword: string): Promise<IJobSeeker[]> {
        var jobseekers: IJobSeeker[];
        if(keyword)
            jobseekers = await JobSeeker.find({ ...filters,  $text: { $search: keyword}});
        else
            jobseekers = await JobSeeker.find(filters);
        AppLogger.info(`Fetch search and filtered jobseekers`);
        return jobseekers;
    }
}