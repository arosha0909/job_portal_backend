import { AppLogger } from "../common/logging";
import { StringOrObjectId } from "../common/util";
import { DVacancy, FVacancy, IVacancy } from "../models/vacancy-model";
import Vacancy from "../schemas/vacancy-schema";

export namespace VacancyDao {

    // employee
    export async function createVacancy(data: Partial<DVacancy>): Promise<IVacancy> {
        const iVacancy = new Vacancy(data);
        let vacancy = await iVacancy.save();
        AppLogger.info(`Create vacancy of vacancy ID: ${vacancy._id}`);
        return vacancy;
    }

    // employee
    export async function updateVacancy(vacancyID: StringOrObjectId, userID: StringOrObjectId, data: Partial<DVacancy>): Promise<IVacancy> {
        console.log(vacancyID, userID);
        // validate vacancy created author/ employee and update
        const updatedVacancy = await Vacancy.findOneAndUpdate({_id:vacancyID, user: userID}, {'$set': data}, {new: true, useFindAndModify: false});
        AppLogger.info(`Update vacancy of vacancy ID: ${updatedVacancy._id}`);
        return updatedVacancy;
    }

    // public or employee
    export async function viewVacanciesPostedByEmployee(userID: StringOrObjectId): Promise<IVacancy[]> {
        const vacancies = await Vacancy.find({user: userID});
        AppLogger.info(`Fetch all vacancies posted by employee ${userID}`);
        return vacancies;
    }

    // public or Employee
    export async function viewVacancyPostedByEmployee(employeeID: StringOrObjectId, vacancyID : StringOrObjectId): Promise<IVacancy> {
        const vacancy = await Vacancy.findOne({_id: vacancyID, user: employeeID});
        AppLogger.info(`Fetch vacancy with vacancy id ${vacancyID} posted by employee id ${employeeID}`);
        return vacancy;
    }

    // public
    export async function searchVacancies(filters: Partial<FVacancy>, keyword: string): Promise<IVacancy[]> {
        var vacancies: IVacancy[];
        if(keyword)
            vacancies = await Vacancy.find({ ...filters,  $text: { $search: keyword}});
        else
            vacancies = await Vacancy.find(filters);
        AppLogger.info(`Fetch search and filtered vacancies`);
        return vacancies;
    }

    // public
    export async function viewVacancies(): Promise<IVacancy[]> {
        const vacancies = await Vacancy.find();
        AppLogger.info(`Fetch all vacancies`);
        return vacancies;
    }

    // public
    export async function viewVacancy(vacancyID: StringOrObjectId): Promise<IVacancy> {
        const vacancy = await Vacancy.findById(vacancyID);
        AppLogger.info(`Fetch vacancy with vacancy id ${vacancyID}`);
        return vacancy;
    }

} 