import { DEmployer, FEmployee, IEmployer } from "../models/employee-model";
import Employer from "../schemas/employee-schema";
import { AppLogger } from "../common/logging";
import { UserDao } from "./user-dao";
import { StringOrObjectId } from "../common/util";

export namespace EmployerDao {
    export async function createEmployer(data: DEmployer): Promise<string> {
        const iEmployer = new Employer(data);
        let employer = await iEmployer.save();
        AppLogger.info(`Create profile for user ID: ${employer._id}`);
        return await UserDao.authenticateUser(data.email, data.password);
    }

    export async function updatedEmployer(employerId: StringOrObjectId, data: Partial<DEmployer>): Promise<IEmployer> {
        // @ts-ignore
        const updatedEmployer = await Employer.findByIdAndUpdate(Types.ObjectId(employerId), {'$set': data}, {new: true});
        AppLogger.info(`update employer for ID: ${updatedEmployer.id}`);
        return updatedEmployer;
    }

    
    // public
    export async function searchEmployees(filters: Partial<FEmployee>, keyword: string): Promise<IEmployer[]> {
        var employees: IEmployer[];
        if(keyword)
            employees = await Employer.find({ ...filters,  $text: { $search: keyword}});
        else
            employees = await Employer.find(filters);
        AppLogger.info(`Fetch search and filtered employees`);
        return employees;
    }
} 