import { AppLogger } from "../common/logging";
import { DPasswordReset, IPasswordReset } from "../models/password-reset-model";
import PasswordReset from "../schemas/password-reset-schema";

export namespace PasswordResetDao {
    
    export async function addEmailAndToken(data:DPasswordReset): Promise<IPasswordReset> {
        PasswordReset.find({email: data.email}).remove().exec();
        const iPasswordReset = new PasswordReset(data);
        const passwordReset = await iPasswordReset.save();
        AppLogger.info(`create password reset entry: ${passwordReset}`);
        return passwordReset;
    }

    export async function tokenValidate(email: string, token: string): Promise<IPasswordReset | null> {
        const iPassword: IPasswordReset = await PasswordReset.findOne({email: email, token: token});
        AppLogger.info(`validate token for email: ${email}, ${token}`);
        return iPassword;
    }
}