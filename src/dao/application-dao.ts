import { ApplicationError } from "../common/application-error";
import { AppLogger } from "../common/logging";
import { StringOrObjectId } from "../common/util";
import { IApplication } from "../models/application-model";
import Application from "../schemas/application-schema";
import User from "../schemas/user-schema";
import { VacancyDao } from "./vacancy-dao";

export namespace ApplicationDao {

    // employee
    export async function viewApplicationsFromVacancyAndEmployee(userID: StringOrObjectId, vacancyID: StringOrObjectId): Promise<IApplication[]> {
        // validate vacancy placed by employee
        const vacancy = await VacancyDao.viewVacancyPostedByEmployee(userID, vacancyID);
        console.log(userID)
        if (vacancy) {
            const applications = await Application.find({vacancy: vacancyID }).populate({
                path: "user",
                select: `firstName lastName email`
            });
            AppLogger.info(`Fetch all applications of the vacancy ${vacancyID} posted by employee ${userID}`);
            return applications;
        } else throw new ApplicationError("Invalid vacancy or user does not have enough permisions to view");
    }

    // employee
    export async function viewApplicationFromVacancyAndEmployee(userID: StringOrObjectId, vacancyID: StringOrObjectId, applicationID: StringOrObjectId): Promise<IApplication> {
        // validate vacancy placed by employee
        const vacancy = await VacancyDao.viewVacancyPostedByEmployee(userID, vacancyID);
        if (vacancy) {
            const application = await Application.findOne({ _id: applicationID, vacancy: vacancyID }).populate({
                path: "user",
                select: `-__v -password -role -permissions -lastLogin -createdAt -updatedAt`
            });
            AppLogger.info(`Fetch application of id ${applicationID} of the vacancy ${vacancyID} posted by employee ${userID}`);
            return application;
        } else throw new ApplicationError("Invalid vacancy or user does not have enough permisions to view");
    }

} 