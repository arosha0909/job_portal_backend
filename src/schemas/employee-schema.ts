import * as mongoose from "mongoose";
import { Schema } from "mongoose";
import { IEmployer } from "../models/employee-model";

export const EmployerSchemaOptions: mongoose.SchemaOptions = {
    _id: true,
    id: false,
    timestamps: true,
    skipVersioning: true,
    strict: false,
    discriminatorKey: 'role',
    toJSON: {
        getters: true,
        virtuals: true,
        transform: (doc, ret) => {
            // delete ret._id;
            delete ret.password;
        }
    },
};

export const employeeSchema = new mongoose.Schema({
    industry: {
        type: Schema.Types.String,
        required: false,
    },
    name: {
        type: Schema.Types.String,
        required: false,
    },
    address: {
        type: Schema.Types.String,
        required: false,
    },
    contacatNo: {
        type: Schema.Types.String,
        required: false,
    },
    aboutCompany: {
        type: Schema.Types.String,
        required: false,
    },
    website: {
        type: Schema.Types.String,
        required: false,
    }
}, EmployerSchemaOptions);

employeeSchema.index( { name: 'text', aboutCompany: 'text', website: 'text' } )
const Employer = mongoose.model<IEmployer>('Employer', employeeSchema);
Employer.createIndexes();
export default Employer;