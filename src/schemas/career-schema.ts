import * as mongoose from "mongoose";
import { Schema } from "mongoose";
import { ICareer } from "../models/career-model";

export const CarerSchemaOptions: mongoose.SchemaOptions = {
    _id: true,
    id: false,
    timestamps: true,
    skipVersioning: true,
    strict: false,
    toJSON: {
        getters: true,
        virtuals: true,
        transform: (doc, ret) => {
            // delete ret._id;
            delete ret.password;
        }
    },
};

export const cateerSchema = new mongoose.Schema({
    titel: {
        type: Schema.Types.String,
        required: false,
    },
    company: {
        type: Schema.Types.String,
        required: false,
    },
    contactName: {
        type: Schema.Types.String,
        required: false,
    },
    contactNumber: {
        type: Schema.Types.String,
        required: false,
    },
    startDate: {
        type: Schema.Types.Date,
        required: false,
    },
    endDate: {
        type: Schema.Types.Date,
        required: false,
    }
}, CarerSchemaOptions);

const Career = mongoose.model<ICareer>('Career', cateerSchema);
export default Career;