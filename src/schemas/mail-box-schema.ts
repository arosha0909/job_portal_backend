import * as mongoose from "mongoose";
import { Schema } from "mongoose";
import { IMailBox } from "../models/mail-box-model";
import Upload from "./upload-schema";
import User from "./user-schema";

export const MailBoxSchemaOptions: mongoose.SchemaOptions = {
    _id: true,
    id: false,
    timestamps: true,
    skipVersioning: true,
    strict: false,
    toJSON: {
        getters: true,
        virtuals: true,
        transform: (doc, ret) => {
            // delete ret._id;
            delete ret.password;
        }
    },
};

export const mailBoxSchema = new mongoose.Schema({
    mail: {
        type: Schema.Types.String,
        require: true,
    },
    subject: {
        type: Schema.Types.String,
        require: true,
    },
    body: {
        type: Schema.Types.String,
        require: true,
    },
    user :{
        type: Schema.Types.ObjectId,
        require: true,
        ref: User.modelName
    },
    attachment: [{
        type: Schema.Types.String,
        require: false,
        default: [],
        ref: Upload.modelName
    }],
}, MailBoxSchemaOptions);

const MailBox = mongoose.model<IMailBox>('MailBox', mailBoxSchema);
export default MailBox;