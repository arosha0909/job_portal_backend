import * as mongoose from "mongoose";
import { Schema } from "mongoose";
import { IPasswordReset } from "../models/password-reset-model";

export const SchemaOptions: mongoose.SchemaOptions = {
    _id: true,
    id: false,
    timestamps: true,
    skipVersioning: true,
    strict: false,
    toJSON: {
        getters: true,
        virtuals: true,
        transform: (doc, ret) => {
            // delete ret._id;
            delete ret.password;
        }
    },
};

export const passwordResetSchema = new mongoose.Schema ({
    email: {
        type: Schema.Types.String,
        required: true
    },
    token: {
        type: Schema.Types.String,
        required: true
    }
}, SchemaOptions);

const PasswordReset = mongoose.model<IPasswordReset>('PasswordReset', passwordResetSchema);
export default PasswordReset;
