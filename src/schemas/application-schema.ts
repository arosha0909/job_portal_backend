import * as mongoose from "mongoose";
import { Schema } from "mongoose";
import { IApplication } from "../models/application-model";
import User from "./user-schema";
import Vacancy from "./vacancy-schema";

export const ApplicationSchemaOptions: mongoose.SchemaOptions = {
    _id: true,
    id: false,
    timestamps: true,
    skipVersioning: true,
    strict: false,
    discriminatorKey: 'role',
    toJSON: {
        getters: true,
        virtuals: true,
        transform: (doc, ret) => {
            // delete ret._id;
            delete ret.password;
        }
    },
};

export const applicationSchema = new mongoose.Schema({
    vacancy: {
        type: Schema.Types.ObjectId,
        require: true,
        ref: Vacancy.modelName
    },
    user: {
        type: Schema.Types.ObjectId,
        require: true,
        ref: User.modelName
    }
}, ApplicationSchemaOptions);

const Application = mongoose.model<IApplication>('Application', applicationSchema);
export default Application;