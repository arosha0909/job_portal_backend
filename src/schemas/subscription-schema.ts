import * as mongoose from "mongoose";
import { Schema } from "mongoose";
import { ISubscription } from "../models/subscription-model";
import User from "./user-schema";

export const SubscriptionSchemaOptions: mongoose.SchemaOptions = {
    _id: true,
    id: false,
    timestamps: true,
    skipVersioning: true,
    strict: false,
    toJSON: {
        getters: true,
        virtuals: true,
        transform: (doc, ret) => {
            // delete ret._id;
            delete ret.password;
        }
    },
};

export const subscriptionSchema = new mongoose.Schema({
    amount: {
        type: Schema.Types.Number,
        require: true
    },
    date: {
        type: Schema.Types.Date,
        require: true
    },
    dueDate: {
        type: Schema.Types.Date,
        require: true
    },
    user: {
        type: Schema.Types.ObjectId,
        require: false,
        ref: User.modelName
    }
}, SubscriptionSchemaOptions);

const Subscription = mongoose.model<ISubscription>('Subscription', subscriptionSchema);
export default subscriptionSchema;