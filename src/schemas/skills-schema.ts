import * as mongoose from "mongoose";
import { Schema } from "mongoose";
import { ISkill } from "../models/skills-model";

export const SkillSchemaOptions: mongoose.SchemaOptions = {
    _id: true,
    id: false,
    timestamps: true,
    skipVersioning: true,
    strict: false,
    toJSON: {
        getters: true,
        virtuals: true,
        transform: (doc, ret) => {
            // delete ret._id;
            delete ret.password;
        }
    },
};

export const SkillSchema = new mongoose.Schema({
    skillName: {
        type: Schema.Types.String,
        required: false,
    }
}, SkillSchemaOptions);

const Skill = mongoose.model<ISkill>('Skill', SkillSchema);
export default Skill;

