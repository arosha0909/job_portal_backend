import * as mongoose from "mongoose";
import { Schema } from "mongoose";
import { IVacancy } from "../models/vacancy-model";
import Address from "./address-schema";
import User from "./user-schema";

export const VacancySchemaOptions: mongoose.SchemaOptions = {
    _id: true,
    id: false,
    timestamps: true,
    // skipVersioning: true,
    // strict: false,
    // discriminatorKey: 'role',
    // toJSON: {
    //     getters: true,
    //     virtuals: true,
    //     transform: (doc, ret) => {
    //         // delete ret._id;
    //         delete ret.password;
    //     }
    // },
};

export const vacancySchema = new mongoose.Schema({
    jobTitle: {
        type: Schema.Types.String,
        require: true,
    },
    jobCategory: {
        type: Schema.Types.String,
        require: true,
    },
    state: {
        type: Schema.Types.Boolean,
        require: true,
        default: false
    },
    status: {
        type: Schema.Types.Boolean,
        require: true,
        default: false
    },
    description: {
        type: Schema.Types.String,
        require: true,
    },
    expirience: {
        type: Schema.Types.String,
        require: true,
    },
    salary: {
        type: Schema.Types.String,
        require: true,
    },
    postDate: {
        type: Schema.Types.Date,
        require: true,
    },
    endDate: {
        type: Schema.Types.Date,
        require: true,
    },
    user: {
        type: Schema.Types.ObjectId,
        require: false,
        ref: User.modelName
    },
    address: {
        type: Schema.Types.Date,
        require: false,
        ref: Address.modelName
    }
}, VacancySchemaOptions);

vacancySchema.index( { jobTitle: 'text', description: 'text' } )


const Vacancy = mongoose.model<IVacancy>('Vacancy', vacancySchema);
Vacancy.createIndexes();
export default Vacancy;