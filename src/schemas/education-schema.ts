import * as mongoose from "mongoose";
import { Schema } from "mongoose";
import { IEducation } from "../models/education-model";
import User from "./user-schema";

export const EducationSchemaOptions: mongoose.SchemaOptions = {
    _id: true,
    id: false,
    timestamps: true,
    skipVersioning: true,
    strict: false,
    toJSON: {
        getters: true,
        virtuals: true,
        transform: (doc, ret) => {
            // delete ret._id;
            delete ret.password;
        }
    },
};

export const educationSchema = new mongoose.Schema({
    titel: {
        type: Schema.Types.String,
        required: false,
    },
    collage: {
        type: Schema.Types.String,
        required: false,
    },
    gpa: {
        type: Schema.Types.Number,
        required: false,
    },
    stratDate: {
        type: Schema.Types.Date,
        required: false,
    },
    EndDate: {
        type: Schema.Types.Date,
        required: false,
    },
    user: {
        type: Schema.Types.ObjectId,
        require: false,
        ref: User.modelName
    },
}, EducationSchemaOptions);

const Education = mongoose.model<IEducation>('Education', educationSchema);
export default Education