import * as mongoose from "mongoose";
import { Schema } from "mongoose";
import { IAddress } from "../models/address-model";

export const AddressSchemaOptions: mongoose.SchemaOptions = {
    _id: true,
    id: false,
    timestamps: true,
    skipVersioning: true,
    strict: false,
    toJSON: {
        getters: true,
        virtuals: true,
        transform: (doc, ret) => {
            // delete ret._id;
            delete ret.password;
        }
    },
};

export const addressSchema = new mongoose.Schema({
    addressOne: {
        type: Schema.Types.String,
        require: false
    },
    addressTwo: {
        type: Schema.Types.String,
        require: false
    },
    city: {
        type: Schema.Types.String,
        require: false
    },
    province: {
        type: Schema.Types.String,
        require: false
    },
    zip: {
        type: Schema.Types.String,
        require: false
    }
}, AddressSchemaOptions);

const Address = mongoose.model<IAddress>('Address', addressSchema);
export default Address;