import * as mongoose from "mongoose";
import { Schema } from "mongoose";
import { IJobSeeker } from "../models/jobSeeker-model";
import { Role } from "../models/user-model";
import Career from "./career-schema";
import Education from "./education-schema";
import Adrress from "./address-schema";
import Skill from "./skills-schema";
import Upload from "./upload-schema";
import User, { UserSchemaOptions } from "./user-schema";
import Address from "./address-schema";

export const jobSeekerSchema = new mongoose.Schema({
    firstName: {
        type: Schema.Types.String,
        require: true,
    },
    lastName: {
        type: Schema.Types.String,
        require: true
    },
    contact: {
        type: Schema.Types.String,
        require: true
    },
    dob: {
        type: Schema.Types.Date,
        require: true
    },
    age: {
        type: Schema.Types.Number,
        require: true
    },
    aboutMe: {
        type: Schema.Types.String,
        require: false
    },
    gender: {
        type: Schema.Types.String,
        require: true
    },
    career: [{
        type: Schema.Types.ObjectId,
        require: false,
        default: [],
        ref: Career.modelName,
    }],
    education: [{
        type: Schema.Types.ObjectId,
        require: false,
        default: [],
        ref: Education.modelName
    }],
    skill: [{
        type: Schema.Types.ObjectId,
        require: false,
        default: [],
        ref: Skill.modelName
    }],
    address: {
        type: Schema.Types.ObjectId,
        require: false,
        ref: Address.modelName
    },
    resumeUpload: {
        type: Schema.Types.ObjectId,
        require: false,
        ref: Upload.modelName
    },
    coverLatter: {
        type: Schema.Types.ObjectId,
        require: false,
        ref: Upload.modelName
    }
}, UserSchemaOptions);

jobSeekerSchema.index({ age: 'text', gender: 'text' })

const JobSeeker = User.discriminator<IJobSeeker>('JobSeeker', jobSeekerSchema, Role.JOB_SEEKER);
JobSeeker.createIndexes();
export default JobSeeker;