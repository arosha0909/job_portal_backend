import * as mongoose from "mongoose";
import {Schema} from "mongoose";
export const e_mailboxSchema = new mongoose.Schema({

    e_mailboxSchemaId: {
        type: Schema.Types.Number,
        required: false,
    },
    employee :{
        type : mongoose.Schema.Types.ObjectId,
        ref:'employeeModel'
    },

    email: {
        type: Schema.Types.String,
        required: false,
    },
    subject: {
        type: Schema.Types.String,
        required: false,
    },
    cont_name: {
        type: Schema.Types.String,
        required: false,
    },
    cont_num: {
        type: Schema.Types.String,
        required: false,
    },
    start_date: {
        type: Schema.Types.Date,
        required: false,
    },
    end_date: {
        type: Schema.Types.Date,
        required: false,
    }

   
},
//code
);